#!/bin/sh

echo Current Issues:
grep -R \ ACMPL_ISSUE.\* st-autocomplete -o -n | tee issues.log
diff -uraN st-0.8.5 st-autocomplete > test-releases/st-0.8.5-autocomplete-$(date +%Y%m%d-%H%M%S)-testrelease.diff
